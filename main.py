#!/usr/bin/env python

import urllib.request
import telegram
import logging
import pathlib
import os
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from pydub import AudioSegment

TOKEN = '828993267:AAHxEfEvnXUhQYXFboch8rbKxHFHYerY5Os'
DOWNLOAD_PATH = '{}/Downloads/to_mp_three_bot'.format(pathlib.Path.home())

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
updater = Updater(token=TOKEN, use_context=True)
dispatcher = updater.dispatcher
bot = telegram.Bot(token=TOKEN)

def convert_audio(file_name, new_format='mp3'):
    split_file_name = os.path.splitext(file_name)
    file_format = split_file_name[1][1:]
    new_file_name = '{}.{}'.format(split_file_name[0], new_format)

    AudioSegment.from_file(file_name, file_format).export(new_file_name, new_format)

    return new_file_name

def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please send me a file!")

start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

def upload(update, context):
    if not update.message.document:
        return

    uploaded_file = bot.getFile(update.message.document.file_id)
    file_name = '{}/{}'.format(DOWNLOAD_PATH, update.message.document.file_name)

    urllib.request.urlretrieve(
        uploaded_file.file_path,
        file_name
    )

    new_file_name = convert_audio(file_name)
    context.bot.send_audio(chat_id=update.effective_chat.id, audio=open(new_file_name, 'rb'))

    os.remove(file_name)
    os.remove(new_file_name)

upload_handler = MessageHandler(Filters.all, upload)
dispatcher.add_handler(upload_handler)

updater.start_polling()
